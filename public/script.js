let can=document.getElementById("can");
let c=can.getContext("2d");
let angle=0;
c.lineWidth=3;
c.fillStyle="#f00";

let circle=(x,y,radius)=>{
    c.beginPath();
    c.ellipse(x, y, radius, radius, Math.PI, 0, 2*Math.PI);
    c.strokeStyle="#fff";
    c.stroke();
    c.closePath();
}
let line=(startx,starty,endx,endy)=>{
    c.beginPath();
    c.moveTo(startx,starty);
    c.lineTo(endx,endy);
    c.strokeStyle="#fff";
    c.stroke();
    c.closePath();
}

c.translate(can.width/2,can.height/2);
setInterval(()=>{
    c.clearRect(-can.width/2,-can.width/2,can.width,can.height);
    x=y=0;    
    for(let i=1;i<document.getElementById("slider").value;i+=2){
        prevx=x;prevy=y;
        radius=100*(4/(i*Math.PI));
        x+=radius * Math.cos(i*angle*Math.PI/180)
        y+=radius * Math.sin(i*angle*Math.PI/180)
        circle(prevx,prevy,radius);
        line(prevx, prevy, x, y);
    }
    if(angle==360) angle=0; else angle++;
},1000/60);